'use strict';

angular.module('app')
	.controller('sessionController', [ '$rootScope', '$scope', '$http', '$state',
	                                function($rootScope, $scope, $http, $state) {
		$scope.title = '会话管理';
        $scope.session = { };
        $scope.loading = false;
        
		$scope.search = function () {
	        $scope.loading = true;
			$.ajax({
				url : '/session/read/list',
				data: $scope.session
			}).then(function(result) {
		        $scope.loading = false;
				if (result.httpCode == 200) {
					$scope.pageInfo = result.data;
				} else {
					$scope.msg = result.msg;
				}
				$scope.$apply();
			});
		}
		
		$scope.search();
		
		$scope.clearSearch = function() {
			$scope.session.keyword= null;
			$scope.search();
		}
		
		$scope.disableItem = function(id, account) {
			if(account != $rootScope.userInfo.account || confirm('确认要自杀？')){
				$.ajax({
					url : '/session/delete',
					data: {'id': id}
				}).then(function(result) {
			        $scope.loading = false;
					if (result.httpCode == 200) {
						$state.reload();
					} else {
						$scope.msg = result.msg;
					}
				});
			}
		}
		
		// 翻页
        $scope.pagination = function (page) {
            var params = {pageIndex: page};
            // 合并搜索字段
            if($scope.keyword) {
                angular.extend(params, $scope.user);
            }
            go(params);
        };
} ]);